package com.example.roundy;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CreateSpaceFragment extends Fragment {

    FirebaseFirestore db;
    FirebaseStorage storage;
    StorageReference storageReference;

    private Button confirmBtn;
    private TextView addImageBtn;
    private EditText spaceName, spaceCode;
    private boolean codeExists, imageAdded = false;
    private ImageView spaceImage;
    private Uri filePath;
    private String imageId;

    public static final int PICK_IMAGE = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)  {
        View view=inflater.inflate(R.layout.fragment_create, container, false);
        confirmBtn = view.findViewById(R.id.confirm_create);
        addImageBtn = view.findViewById(R.id.add_space_image);
        spaceImage = view.findViewById(R.id.space_image);
        spaceName = view.findViewById(R.id.snameET);
        spaceCode = view.findViewById(R.id.scodeET);
        db = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        confirmBtn.setEnabled(true);

        //confirmBtn.setEnabled(true);
        setupListeners();
        return view;
    }

    private void setupListeners() {

        addImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageAdded = true;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
            }
        });

        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //confirmBtn.setEnabled(false);

                boolean valid = true, codeExists = true;
                if(spaceName.getText().toString().trim().isEmpty())
                {       spaceName.setError("Please insert a name");
                    valid = false;
                }

                if(spaceCode.getText().toString().trim().isEmpty()) {
                    spaceCode.setError("Please insert a code");
                    valid =false;
                }
                if(!imageAdded) {
                    addImageBtn.setText("Click here to insert an image!");
                    addImageBtn.setTextColor(Color.RED);
                    valid =false;
                }
                if (valid)
                    codeExists = doesCodeExist(spaceCode.getText().toString().trim());
                if(valid && !codeExists){
                    //create a space here
                    uploadImage();

                    Map<String, Object> space = new HashMap<>();
                    space.put("name",spaceName.getText().toString().trim() );
                    space.put("creator", "/users/"+UserData.userID+"/");
                    space.put("image", "/images/"+imageId+".jpg");

                    String newSpaceCode = spaceCode.getText().toString().trim();

                    db.collection("spaces").document(newSpaceCode)
                            .set(space)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    confirmBtn.setEnabled(false);
                                    DocumentReference spaceRef = db.collection("spaces").document(spaceCode.getText().toString().trim());
                                    spaceRef.update("members", FieldValue.arrayUnion("/users/"+UserData.userID+"/"))
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void unused) {
                                                    Toast.makeText(getActivity(), "Space created successfully!",Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(getActivity(), "Error Creating Space",Toast.LENGTH_SHORT).show();
                                }
                            });
                    /*HomeFragment homeFragment = new HomeFragment();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragMainContent,homeFragment);
                    fragmentTransaction.commit();*/
                }
                    if( valid && codeExists)
                        spaceCode.setError("Code already exists");
                }
        });
    }

    private void uploadImage() {
        imageId = "";
        imageId = UUID.randomUUID().toString();
        StorageReference ref = storageReference.child("images/" + imageId+".jpg");
        ref.putFile(filePath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Log.d("slm","success!!!");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("slm",e.toString());
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        filePath = data.getData();
        if (requestCode == PICK_IMAGE) {
            spaceImage.setImageURI(filePath);
            addImageBtn.setText("Add an image");
            addImageBtn.setTextColor(Color.BLUE);
        }
    }

    private boolean doesCodeExist(String trim) {
            db.collection("spaces").document(spaceCode.getText().toString().trim()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            //Toast.makeText(getActivity(), "DocumentSnapshot data: " + document.getData(), Toast.LENGTH_SHORT);
                            codeExists = true;
                        } else {
                            //Toast.makeText(getActivity(), "No such document", Toast.LENGTH_SHORT);
                            codeExists = false;
                        }
                    }
                    }});
        return codeExists;
    }

}
