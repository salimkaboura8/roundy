package com.example.roundy;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

public class JoinSpaceFragment extends Fragment {

    private EditText joinCode;
    private Button createBtn;
    private ImageButton joinBtn;
    FirebaseFirestore db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)  {
        View view=inflater.inflate(R.layout.fragment_join_space, container, false);

        db = FirebaseFirestore.getInstance();
        createBtn = (Button) view.findViewById(R.id.create_space_button);
        joinBtn = (ImageButton)     view.findViewById(R.id.join_space_button);
        joinCode = (EditText) view.findViewById(R.id.join_space_code);

        setupListeners();
        return view;
    }

    private void setupListeners() {
        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment createFragment = new CreateSpaceFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction().replace(R.id.fragMainContent, createFragment);
                fragmentTransaction.commit();
            }
        });
        joinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String spaceCode = joinCode.getText().toString().trim();
                Log.d("slm","in onclick");
                if (!spaceCode.isEmpty()){
                    DocumentReference spaceRef = db.collection("spaces").document(spaceCode);
                    spaceRef.update("members", FieldValue.arrayUnion("/users/"+LoginActivity.currentUserId+"/"))
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void unused) {
                            joinCode.setText("");
                            HomeFragment homeFragment = new HomeFragment();
                            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.fragMainContent,homeFragment);
                            fragmentTransaction.commit();
                            Toast.makeText(getActivity(), "You've joined a new Space!", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Wrong Code, please try again", Toast.LENGTH_SHORT).show();
                        }
                    });
            }
                else{
                    Toast.makeText(getActivity(), "Please Insert a Code", Toast.LENGTH_SHORT).show();
                }
        }});
    }
}