package com.example.roundy;

import android.net.Uri;
import android.util.Log;
import android.widget.Space;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.storage.FirebaseStorage;

import java.util.ArrayList;
import java.util.List;

public class UserData {

    public static String userID;
    public static Uri userImage = null;
    public static String userName;
    public static String userEmail;
    public static String userPassword;
    public static List<DocumentSnapshot> userSpaces = null;
    public static List<SpaceModel> userSpacesList = new ArrayList<>();
    public static DocumentSnapshot userDocument = null;
    public static DocumentSnapshot currentSpace;

}
