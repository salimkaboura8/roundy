package com.example.roundy;

import android.app.Activity;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

public class PostListAdapter  extends ArrayAdapter<String> {
    FirebaseStorage storage;
    StorageReference storageRef;
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    private Activity context;
    TextView author;
    ImageView picture_author;
    TextView post_date;
    TextView content;
    ImageView img_post;
    private List<DocumentSnapshot> spacePosts;
    //private List<Post> posts;
    public PostListAdapter(Activity context, List<DocumentSnapshot> posts) {
        super(context, R.layout.custom_list_space_page);
        this.context = context;
        this.spacePosts = posts;
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference();
    }
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.custom_list_space_page, null,true);
        picture_author = rowView.findViewById(R.id.author_pic);
        author = rowView.findViewById(R.id.author_name);
        post_date = rowView.findViewById(R.id.post_date);
        content = rowView.findViewById(R.id.post_content);
        img_post = rowView.findViewById(R.id.image_post);

        if(spacePosts.get(position).get("image").toString() != "null")
            linkImage(spacePosts.get(position).get("image").toString(), img_post);
        else{
            img_post.setVisibility(View.INVISIBLE);
        }
        writeAuthorNameImage(author, spacePosts.get(position).get("author").toString());
        post_date.setText(spacePosts.get(position).get("date").toString());
        content.setText(spacePosts.get(position).get("text").toString());
        return rowView;
    }



    private void writeAuthorNameImage(TextView authorTV, String author) {
        //Toast.makeText(context, author, Toast.LENGTH_SHORT).show();
        db.collection("users").document(author).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                authorTV.setText(documentSnapshot.get("full_name").toString());
                linkImage(documentSnapshot.get("image").toString(),picture_author);
            }
        });
    }

    private void linkImage(String imagePath, ImageView view) {
        storage.getReference().child(imagePath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(context).load(uri).into(view);
            }
        });
    }

    @Override
    public int getCount() {
        return spacePosts.size();
    }
}
