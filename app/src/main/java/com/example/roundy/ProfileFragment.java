package com.example.roundy;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.auth.User;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.UUID;

public class ProfileFragment  extends Fragment {

    private TextView emailTV, deleteBtn, nameTV;
    private Button changePasswordBtn;
    private ImageView profilePic;
    FirebaseFirestore db;
        FirebaseStorage storage = FirebaseStorage.getInstance();
        AlertDialog.Builder alertDialogBuilder;
        public static final int PICK_IMAGE = 1;
        private Uri filePath;
        private String imageId;


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState)  {
            View view=inflater.inflate(R.layout.fragment_profile, container, false);

            db = FirebaseFirestore.getInstance();
            nameTV = (TextView) view.findViewById(R.id.full_name);
            emailTV = (TextView) view.findViewById(R.id.emailTV);
            deleteBtn = (TextView) view.findViewById(R.id.delete_button);
            profilePic = view.findViewById(R.id.profile_pic);
            changePasswordBtn = (Button) view.findViewById(R.id.change_password_button);
            if(UserData.userImage == null) {
                getProfilePic();
            }
            else{
                Glide.with(getActivity()).load(UserData.userImage).into(profilePic);
            }

            alertDialogBuilder = new AlertDialog.Builder(getActivity());
            alertDialogBuilder.setTitle("Confirm delete ?");
            alertDialogBuilder.setMessage("Are you sure you want to delete your account ?");
            alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
            public void onClick(DialogInterface arg0, int arg1) {
                db.collection("users").document(LoginActivity.currentUserId)
                        .delete()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(getActivity(), "Your account has been deleted", Toast.LENGTH_SHORT).show();
                                PackageManager packageManager = getActivity().getPackageManager();
                                Intent intent = packageManager.getLaunchIntentForPackage(getActivity().getPackageName());
                                ComponentName componentName = intent.getComponent();
                                Intent mainIntent = Intent.makeRestartActivityTask(componentName);
                                getActivity().startActivity(mainIntent);
                                Runtime.getRuntime().exit(0);
                                //Log.d( "DocumentSnapshot successfully deleted!");
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getActivity(), "Your account has been deleted", Toast.LENGTH_SHORT).show();
                                //Log.w(TAG, "Error deleting document", e);
                            }
                        });            }
        });


        alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        fillPage();
        setupListeners();

        return view;
    }

    private void getProfilePic() {
            if( UserData.userDocument.get("image") != null) {

            storage.getReference().child(UserData.userDocument.get("image").toString())
                    .getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Glide.with(getActivity()).load(uri).into(profilePic);
                    UserData.userImage = uri;
                }
            });
        }
    }

    private void fillPage() {
        nameTV.setText(UserData.userName);
        emailTV.setText(UserData.userEmail);
    }

    private void setupListeners() {

        changePasswordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment passwordFragment = new PasswordFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction().replace(R.id.fragMainContent, passwordFragment);
                fragmentTransaction.commit();
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                Button positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                positiveButton.setTextColor(Color.parseColor("#0000ff"));
                Button negativeButton = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                negativeButton.setTextColor(Color.parseColor("#0000ff"));
            }
        });

        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        filePath = data.getData();
        if (requestCode == PICK_IMAGE) {
            UserData.userImage = filePath;
            profilePic.setImageURI(filePath);
            uploadImage();
        }
    }
    private void uploadImage() {
        imageId = "";
        imageId = UUID.randomUUID().toString();
        StorageReference ref = storage.getReference().child("images/" + imageId+".jpg");
        ref.putFile(filePath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                db.collection("users").document(UserData.userID)
                        .update("image","/images/"+imageId+".jpg").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        db.collection("users").document(UserData.userID).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                UserData.userDocument = documentSnapshot;
                            }
                        });
                        Toast.makeText(getActivity(),"Your profile picture has been updated!",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("slm",e.toString());
            }
        });
    }
}
