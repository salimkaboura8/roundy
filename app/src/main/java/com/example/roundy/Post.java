package com.example.roundy;

import android.net.Uri;

import java.io.Serializable;
import java.net.URI;

public class Post implements Serializable {
    private String content;
    private Uri srcImg;
    private String date;
    private Uri pictureUser;
    private String author;
    public Post(String content, Uri srcImg, String date, Uri pictureUser, String author) {
        this.content = content;
        this.srcImg = srcImg;
        this.date = date;
        this.pictureUser = pictureUser;
        this.author = author;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Uri getPictureUser() {
        return pictureUser;
    }

    public void setPictureUser(Uri pictureUser) {
        this.pictureUser = pictureUser;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Uri getSrcImg() {
        return srcImg;
    }

    public void setSrcImg(Uri srcImg) {
        this.srcImg = srcImg;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
