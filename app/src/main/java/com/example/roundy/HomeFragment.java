package com.example.roundy;

import android.app.FragmentTransaction;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import android.app.Fragment;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Space;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.auth.User;
import com.google.firebase.storage.FirebaseStorage;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    FirebaseFirestore db;
    FirebaseStorage storage;
    private List<DocumentSnapshot> myspaceslist;
    private ListView listSpaces;
    private ProgressBar progressBar;
    HomeListAdapter adapter;
    private int spacesCount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)  {
        View result=inflater.inflate(R.layout.fragment_home, container, false);

        db = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        listSpaces = (ListView) result.findViewById(R.id.listSpaces);
        progressBar = result.findViewById(R.id.progress_bar);

        if (UserData.userSpacesList.size()>0){
            progressBar.setVisibility(View.GONE);
            HomeListAdapter adapter = new HomeListAdapter(getActivity(), UserData.userSpacesList);
            listSpaces.setAdapter(adapter);
            listSpaces.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    UserData.currentSpace = UserData.userSpacesList.get(position).spaceDoc;
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragMainContent, new SpaceFragment());
                    fragmentTransaction.commit();
                }
            });
        }else {
            db.collection("spaces")
                    .whereArrayContains("members", "/users/" + LoginActivity.currentUserId + "/")
                    .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                @Override
                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                    myspaceslist = queryDocumentSnapshots.getDocuments();

                    setSpacesList(myspaceslist);
                    //UserData.userSpaces = myspaceslist;

                    adapter = new HomeListAdapter(getActivity(), UserData.userSpacesList);
                    listSpaces = (ListView) result.findViewById(R.id.listSpaces);
                    listSpaces.setAdapter(adapter);
                    listSpaces.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            UserData.currentSpace = UserData.userSpacesList.get(position).spaceDoc;
                            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.fragMainContent, new SpaceFragment());
                            fragmentTransaction.commit();
                        }
                    });
                }
            });
        }

        return result;
    }

    private void setSpacesList(List<DocumentSnapshot> myspaceslist) {
                spacesCount = myspaceslist.size();
                for(DocumentSnapshot spaceDoc : myspaceslist){
                    saveSpaceToList(spaceDoc);
                }
    }

    private void saveSpaceToList(DocumentSnapshot spaceDoc) {
        storage.getReference().child(spaceDoc.get("image").toString()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                UserData.userSpacesList.add(new SpaceModel(spaceDoc, uri));
                spacesCount--;
                adapter.notifyDataSetChanged();
                if(spacesCount==0)
                    progressBar.setVisibility(View.GONE);
            }
        });
    }

    private ArrayList<SpaceDocument> filterSpaces(List<DocumentSnapshot> myspaceslist) {
        ArrayList<SpaceDocument> myspacesfiltered = new ArrayList<>();

        for (DocumentSnapshot doc : myspaceslist){
            String imagePath = doc.get("image").toString();
            storage.getReference().child(imagePath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    SpaceDocument mydoc = new SpaceDocument(doc.get("name").toString(), uri);
                    myspacesfiltered.add(mydoc);
                }
            });
        }
        return myspacesfiltered;
    }

}
