package com.example.roundy;

import static android.app.Activity.RESULT_OK;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.common.net.InternetDomainName;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.auth.User;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


public class SpaceFragment extends Fragment {

    private TextView headerName;
    private ImageView headerImage;

    private List<DocumentSnapshot> posts;

    private ListView listSpaces;
    private ImageView addImageButton;
    private ImageView addedImage;
    private LinearLayout emptySpace;
    private ImageView profilePic;
    private String imageId;
    private Button postButton;
    private EditText postContent;
    public static final int PICK_IMAGE = 1;

    FirebaseFirestore db;
    FirebaseStorage storage;
    private Uri filePath;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.fragment_space, container, false);

        db = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();

        headerName = result.findViewById(R.id.header_name);
        headerImage = result.findViewById(R.id.header_image);
        profilePic = result.findViewById(R.id.profile_pic);
        addedImage = result.findViewById(R.id.added_image);
        emptySpace = result.findViewById(R.id.empty_space);
        getSpaceHeader();
        getSpacePosts();
        if(UserData.userDocument.get("image")!=null){
            linkImage(UserData.userDocument.get("image").toString(),profilePic);
        }
        listSpaces = (ListView) result.findViewById(R.id.list_posts);
        addImageButton = (ImageView) result.findViewById(R.id.add_image_button);
        postButton = (Button) result.findViewById(R.id.sg_post);
        postContent = (EditText) result.findViewById(R.id.postContent);
        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String textPost = postContent.getText().toString().trim();
                if (textPost.length() == 0) {
                    postContent.setError("Field is required");
                } else {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    Date date = new Date();
                    System.out.println(formatter.format(date));
                    //TODO
                    Map<String, Object> post = new HashMap<>();
                    post.put("text", textPost);
                    post.put("author", UserData.userID);
                    post.put("date", formatter.format(date));
                    post.put("space", "/spaces/" + UserData.currentSpace.getId());

                    if (filePath != null) {
                        uploadImage();
                        post.put("image", "/images/" + imageId + ".jpg");
                    } else {
                        post.put("image", "null");
                    }
                    db.collection("posts").document()
                            .set(post)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void unused) {
                                    postContent.setText("");
                                    addedImage.setImageBitmap(null);
                                    getSpacePosts();
                                    Toast.makeText(getActivity(), "Posted successfully!", Toast.LENGTH_SHORT).show();
                                }
                            });
                }
            }
        });

        addImageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
            }
        });

        return result;
    }

    private void getSpacePosts() {
        db.collection("posts").whereEqualTo("space", "/spaces/" + UserData.currentSpace.getId())
                .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                posts = queryDocumentSnapshots.getDocuments();
                if(posts.size()==0){
                    emptySpace.setVisibility(View.VISIBLE);
                }
                else{
                    emptySpace.setVisibility(View.INVISIBLE);
                    Collections.reverse(posts);
                    PostListAdapter adapter = new PostListAdapter(getActivity(), posts);
                    listSpaces.setFriction(ViewConfiguration.getScrollFriction() * 3);
                    listSpaces.setAdapter(adapter);
                }
            }
        });
    }

    private void getSpaceHeader() {
        headerName.setText(UserData.currentSpace.get("name").toString());
        if (UserData.currentSpace.get("image") != null) {
            storage.getReference().child(UserData.currentSpace.get("image").toString())
                    .getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Glide.with(getActivity()).load(uri).into(headerImage);
                }
            });
        }
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        if (data!=null)
            filePath = data.getData();
        if (resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                addedImage.setImageBitmap(selectedImage);
                final float scale = getActivity().getResources().getDisplayMetrics().density;
                addedImage.getLayoutParams().height = (int) (100 * scale + 0.5f);
                addedImage.getLayoutParams().width = (int) (100 * scale + 0.5f);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getActivity(), "You haven't picked Image", Toast.LENGTH_LONG).show();
        }
    }

    private void uploadImage() {
        imageId = "";
        imageId = UUID.randomUUID().toString();
        StorageReference ref = storage.getReference().child("images/" + imageId + ".jpg");
        ref.putFile(filePath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Log.d("slm", "success!!!");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("slm", e.toString());
            }
        });
    }



    private void linkImage(String imagePath, ImageView view) {
        storage.getReference().child(imagePath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(getActivity()).load(uri).into(view);
            }
        });
    }
}