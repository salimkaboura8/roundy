package com.example.roundy;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainPageActivity  extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;
    private HomeFragment homeFragment;
    private ProfileFragment profileFragment;
    private JoinSpaceFragment joinSpaceFragment;
    private Button createBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupListeners();

        homeFragment = new HomeFragment();
        joinSpaceFragment = new JoinSpaceFragment();
        profileFragment = new ProfileFragment();
        setContentView(R.layout.activity_main_page);
        bottomNavigationView = findViewById(R.id.activity_main_bottom_navigation);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragMainContent,joinSpaceFragment);
        bottomNavigationView.setSelectedItemId(R.id.action_add);
        fragmentTransaction.commit();
        this.configureBottomView();
    }

    private void setupListeners() {

    }

    private void configureBottomView() {
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> updateMainFragment(item.getItemId()));
    }
    private boolean updateMainFragment(int itemId) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        switch (itemId) {
            case R.id.action_home:
                fragmentTransaction.replace(R.id.fragMainContent,homeFragment);
                fragmentTransaction.commit();
                break;
            case R.id.action_add:
                fragmentTransaction.replace(R.id.fragMainContent,joinSpaceFragment);
                fragmentTransaction.commit();
                break;
            case R.id.action_profile:
                fragmentTransaction.replace(R.id.fragMainContent,profileFragment);
                fragmentTransaction.commit();
                break;
        }
        return true;
    }

}
