package com.example.roundy;

import android.net.Uri;

public class SpaceDocument {

    String title;
    Uri imageUri;

    public SpaceDocument(String title, Uri imageUri) {
        this.title = title;
        this.imageUri = imageUri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Uri getImageUri() {
        return imageUri;
    }

    public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }
}
