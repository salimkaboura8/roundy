package com.example.roundy;

import android.app.Activity;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

public class HomeListAdapter extends ArrayAdapter<String> {

    private Activity context;
    private List<SpaceModel> myspaces;
    private TextView spaceTitle;
    private ImageView spaceBackground;
    Uri imageUri =null;
    FirebaseStorage storage;
    StorageReference storageRef;

    public HomeListAdapter(Activity context, List<SpaceModel> myspaceslist) {
        super(context, R.layout.custom_list_home_page);
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference();

        this.context=context;
        myspaces = myspaceslist;
    }
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.custom_list_home_page, null,true);
        spaceTitle = (TextView) rowView.findViewById(R.id.space_title);
        spaceBackground = (ImageView) rowView.findViewById(R.id.space_background);

        spaceTitle.setText(myspaces.get(position).spaceDoc.get("name").toString());
        Glide.with(context).load(myspaces.get(position).spaceImage).into(spaceBackground);

        return rowView;
    }

    private void linkImage(String imagePath, ImageView view) {
        storage.getReference().child(imagePath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(context).load(uri).into(view);
            }
        });
    }

    @Override
    public int getCount() {
        return myspaces.size();
    }
}
