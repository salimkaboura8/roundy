package com.example.roundy;

import android.net.Uri;

import com.google.firebase.firestore.DocumentSnapshot;

public class SpaceModel {
    DocumentSnapshot spaceDoc;
    Uri spaceImage;

    public SpaceModel(DocumentSnapshot spaceDoc, Uri spaceImage){
        this.spaceDoc = spaceDoc;
        this.spaceImage = spaceImage;
    }
}
