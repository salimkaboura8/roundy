package com.example.roundy;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;

public class PasswordFragment extends Fragment {

    FirebaseFirestore db;
    private EditText passwordET, newPassET, confirmPassET;
    private Button confirmBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_password, container, false);
        db = FirebaseFirestore.getInstance();

        passwordET = view.findViewById(R.id.current_password);
        newPassET = view.findViewById(R.id.new_password1);
        confirmPassET = view.findViewById(R.id.new_password2);
        confirmBtn = view.findViewById(R.id.confirm_change_pass);
        setupListeners();
        
        return view;
    }


    private void setupListeners() {
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmBtn.setEnabled(false);
                changePassword();
            }
        });
    }

    private void changePassword() {
        boolean valid = true;

        if (passwordET.getText().toString().isEmpty()){
            passwordET.setError("This field can't be empty");
            valid = false;
        }
        if (newPassET.getText().toString().isEmpty()){
            newPassET.setError("This field can't be empty");
            valid = false;
        }
        if (confirmPassET.getText().toString().isEmpty()){
            confirmPassET.setError("This field can't be empty");
            valid = false;
        }

        if (valid){
            if (!UserData.userPassword.equals(passwordET.getText().toString().trim())) {
            passwordET.setError("Incorrect Password");
                valid = false;
            }

            if (!newPassET.getText().toString().trim().equals(confirmPassET.getText().toString().trim())) {
                confirmPassET.setError("Password must be identical");
                valid = false;
            }
        }

        if(valid){
            if (newPassET.getText().toString().trim().equals(passwordET.getText().toString().trim())) {
                newPassET.setError("New password must be different than the current one");
                valid = false;
            }
        }

        if (valid){
            db.collection("users").document(UserData.userID).update("password",newPassET.getText().toString().trim()).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void unused) {
                    Toast.makeText(getActivity(),"Your password has been updated!",Toast.LENGTH_SHORT).show();
                    emptyFields();
                    UserData.userPassword = newPassET.getText().toString().trim();

                    Fragment profileFragment = new ProfileFragment();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction().replace(R.id.fragMainContent, profileFragment);
                    fragmentTransaction.commit();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getActivity(), "Error. Please check your network", Toast.LENGTH_SHORT).show();
                }
            });
        }

        confirmBtn.setEnabled(true);

    }

    private void emptyFields() {
        passwordET.setText("");
        newPassET.setText("");
        confirmPassET.setText("");
    }
}
