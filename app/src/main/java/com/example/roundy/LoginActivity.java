package com.example.roundy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.auth.User;

public class LoginActivity extends AppCompatActivity {

    FirebaseFirestore db;

    Button loginBtn;
    TextView signupBtn;
    EditText emailET, passwordET;
    public static String currentUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        db = FirebaseFirestore.getInstance();
        getViews();
        setupListeners();
    }

    private void getViews() {
        loginBtn = findViewById(R.id.login_button);
        signupBtn = findViewById(R.id.signup_button);
        emailET = findViewById(R.id.lg_email);
        passwordET = findViewById(R.id.lg_password);
    }

    private void setupListeners() {
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean valid = true;
                if(emailET.getText().toString().trim().isEmpty()){
                    emailET.setError("Please insert your email");
                    valid = false;
                }
                if(passwordET.getText().toString().trim().isEmpty()){
                    passwordET.setError("Please insert your password");
                    valid = false;
                }
                if (valid){
                    db.collection("users").whereEqualTo("email",emailET.getText().toString().trim())
                            .get()
                            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                    if (task.isSuccessful()) {
                                        if(task.getResult().isEmpty())
                                            emailET.setError("User with this email does not exist");
                                        else{
                                        for (QueryDocumentSnapshot document : task.getResult()) {
                                            String pass = (String) document.get("password");
                                            currentUserId = document.getId();
                                            UserData.userID = document.getId();
                                            UserData.userDocument = document;
                                            UserData.userName = document.get("full_name").toString();
                                            UserData.userEmail = document.get("email").toString();
                                            UserData.userPassword = document.get("password").toString();
                                            UserData.userImage = null;
                                            if (pass.equals(passwordET.getText().toString().trim())){

                                                Intent intent = new Intent(getApplicationContext(), MainPageActivity.class);
                                                startActivity(intent);
                                            }
                                            else{
                                                passwordET.setError("Email and password do not match");
                                            }
                                        }}
                                    } else {
                                        Log.d("SLM", "Error getting documents: ", task.getException());
                                    }
                                }
                            });
                    }
            }
        });
        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivity(intent);
            }
        });
    }
}