package com.example.roundy;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

public class SignupActivity extends AppCompatActivity {

    FirebaseFirestore db;
    boolean emailExists;

    Button confirmBtn;
    TextView loginBtn;
    EditText fullNameET, emailET, passwordET, confirmPasswordET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        db = FirebaseFirestore.getInstance();

        getViews();
        setupListeners();
    }

    private void getViews() {
        loginBtn = findViewById(R.id.sg_login);
        confirmBtn = findViewById(R.id.sg_confirm);
        fullNameET = findViewById(R.id.fullNameET);
        emailET = findViewById(R.id.emailET);
        passwordET = findViewById(R.id.passwordET);
        confirmPasswordET = findViewById(R.id.confirmPasswordET);
    }

    private void setupListeners() {
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean valid = true;
                if(emailET.getText().toString().trim().isEmpty())
                {       emailET.setError("Please insert your email");
                        valid = false;
                }

                if(fullNameET.getText().toString().trim().isEmpty()) {
                    fullNameET.setError("Please insert your full name");
                    valid =false;
                }

                if(passwordET.getText().toString().trim().isEmpty())
                {
                    passwordET.setError("Please insert your password");
                    valid = false;
                }

                if(confirmPasswordET.getText().toString().trim().isEmpty() ||
                        !confirmPasswordET.getText().toString().trim().equals(passwordET.getText().toString().trim())) {
                    confirmPasswordET.setError("Passwords should be identical");
                    valid = false;
                }

                boolean doesEmailExist = emailExists(emailET.getText().toString().trim());

                if(valid && !doesEmailExist){
                    Map<String, Object> user = new HashMap<>();
                    user.put("email",emailET.getText().toString().trim() );
                    user.put("password", passwordET.getText().toString().trim());
                    user.put("full_name", fullNameET.getText().toString().trim());

                    db.collection("users").document()
                            .set(user)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    //confirmBtn.setEnabled(false);
                                    Toast.makeText(SignupActivity.this, "User successfully registered!", Toast.LENGTH_SHORT).show();
                                    showLoginPage();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(SignupActivity.this, "Error signing up", Toast.LENGTH_SHORT).show();
                                }
                            });                }
                else{
                    if(doesEmailExist)
                        emailET.setError("User already exists");
                }
            }
        });
    }

    private void showLoginPage() {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
    }

    private boolean emailExists(String email) {
        db.collection("users").whereEqualTo("email", email).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.getResult().isEmpty()) emailExists = false;
                else emailExists = true;
            }
        });
        return emailExists;
    }

}